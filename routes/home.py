from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates

# from jinja2 import Environment, FileSystemLoader
# env = Environment(loader=FileSystemLoader("templates"))


router = APIRouter()

# Create a Jinja2 environment with the path to your templates directory
templates = Jinja2Templates(directory="templates")


@router.get("/")
async def home(request: Request):
    return templates.TemplateResponse(
        request=request,
        name="home.html",
        status_code=200,
    )
