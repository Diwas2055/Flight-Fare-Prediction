import pickle

import pandas as pd
from fastapi import APIRouter, HTTPException, Request

# from jinja2 import Environment, FileSystemLoader
from fastapi.templating import Jinja2Templates

from utils.conversion_body import convert_encoded_into_string

from config.custom_logger import logger

router = APIRouter()

# Create a Jinja2 environment with the path to your templates directory
templates = Jinja2Templates(directory="templates")


@router.route("/predict", methods=["GET", "POST"])
async def predict(request: Request):
    try:
        if request.method == "POST":
            model = pickle.load(open("train_model/flight_fare_prediction.pkl", "rb"))

            body = await request.body()
            body_json = convert_encoded_into_string(body.decode("utf-8"))

            # Date_of_Journey
            date_dep = body_json["Dep_Time"]
            Journey_day = int(pd.to_datetime(date_dep, format="%Y-%m-%dT%H:%M").day)
            Journey_month = int(pd.to_datetime(date_dep, format="%Y-%m-%dT%H:%M").month)
            # print("Journey Date : ",Journey_day, Journey_month)

            # Departure
            Dep_hour = int(pd.to_datetime(date_dep, format="%Y-%m-%dT%H:%M").hour)
            Dep_min = int(pd.to_datetime(date_dep, format="%Y-%m-%dT%H:%M").minute)
            # print("Departure : ",Dep_hour, Dep_min)

            # Arrival
            date_arr = body_json["Arrival_Time"]
            Arrival_hour = int(pd.to_datetime(date_arr, format="%Y-%m-%dT%H:%M").hour)
            Arrival_min = int(pd.to_datetime(date_arr, format="%Y-%m-%dT%H:%M").minute)
            # print("Arrival : ", Arrival_hour, Arrival_min)

            # Duration
            Duration_hour = abs(Arrival_hour - Dep_hour)
            Duration_mins = abs(Arrival_min - Dep_min)
            # print("Duration : ", dur_hour, dur_min)

            # Total Stops
            Total_Stops = int(body_json["stops"])
            # print(Total_stops)
            Airline_MultipleCarriersPremiumEconomy = 0
            Airline_JetAirwaysBusiness = 0

            airline = body_json["airline"]
            if airline == "Jet Airways":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 1
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "IndiGo":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 1
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "Air India":
                Airline_AirIndia = 1
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "Multiple carriers":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 1
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "SpiceJet":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 1
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "Vistara":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 1
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "GoAir":
                Airline_AirIndia = 0
                Airline_GoAir = 1
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 0

            elif airline == "Trujet":
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 0
                Airline_Trujet = 1

            else:
                Airline_AirIndia = 0
                Airline_GoAir = 0
                Airline_IndiGo = 0
                Airline_JetAirways = 0
                Airline_MultipleCarriers = 0
                Airline_SpiceJet = 0
                Airline_Vistara = 0
                Airline_Other = 1
                Airline_Trujet = 0

            Source = body_json["Source"]
            if Source == "Delhi":
                Source_Delhi = 1
                Source_Kolkata = 0
                Source_Mumbai = 0
                Source_Chennai = 0

            elif Source == "Kolkata":
                Source_Delhi = 0
                Source_Kolkata = 1
                Source_Mumbai = 0
                Source_Chennai = 0

            elif Source == "Mumbai":
                Source_Delhi = 0
                Source_Kolkata = 0
                Source_Mumbai = 1
                Source_Chennai = 0

            elif Source == "Chennai":
                Source_Delhi = 0
                Source_Kolkata = 0
                Source_Mumbai = 0
                Source_Chennai = 1

            else:
                Source_Delhi = 0
                Source_Kolkata = 0
                Source_Mumbai = 0
                Source_Chennai = 0

            Source = body_json["Destination"]
            if Source == "Cochin":
                Destination_Cochin = 1
                Destination_Delhi = 0
                Destination_Hyderabad = 0
                Destination_Kolkata = 0

            elif Source == "Delhi":
                Destination_Cochin = 0
                Destination_Delhi = 1
                Destination_Hyderabad = 0
                Destination_Kolkata = 0

            elif Source == "Hyderabad":
                Destination_Cochin = 0
                Destination_Delhi = 0
                Destination_Hyderabad = 1
                Destination_Kolkata = 0

            elif Source == "Kolkata":
                Destination_Cochin = 0
                Destination_Delhi = 0
                Destination_Hyderabad = 0
                Destination_Kolkata = 1

            else:
                Destination_Cochin = 0
                Destination_Delhi = 0
                Destination_Hyderabad = 0
                Destination_Kolkata = 0

            prediction = model.predict(
                [
                    [
                        Total_Stops,
                        Journey_day,
                        Journey_month,
                        Dep_hour,
                        Dep_min,
                        Arrival_hour,
                        Arrival_min,
                        Duration_hour,
                        Duration_mins,
                        Airline_AirIndia,
                        Airline_GoAir,
                        Airline_IndiGo,
                        Airline_JetAirways,
                        Airline_MultipleCarriers,
                        Airline_Other,
                        Airline_SpiceJet,
                        Airline_Vistara,
                        Airline_Trujet,
                        Airline_MultipleCarriersPremiumEconomy,
                        Airline_JetAirwaysBusiness,
                        Source_Delhi,
                        Source_Chennai,
                        Source_Kolkata,
                        Source_Mumbai,
                        Destination_Cochin,
                        Destination_Delhi,
                        Destination_Hyderabad,
                        Destination_Kolkata,
                    ]
                ]
            )

            output = round(prediction[0], 2)

            # # Render the template with the dynamic data and message variable
            # template = env.get_template("home.html")
            # html_content = template.render(
            #     prediction_text="Your Flight price is Rs. {}".format(output)
            # )

            # # Return the rendered HTML content
            # return HTMLResponse(content=html_content, status_code=200)
            return templates.TemplateResponse(
                request=request,
                name="home.html",
                context={"prediction_text": f"Your Flight price is Rs. {output}"},
                status_code=200,
            )

        return templates.TemplateResponse(
            request=request,
            name="home.html",
            status_code=200,
        )
    except Exception as e:
        logger.error(f"An error occurred: {str(e)}")
    return HTTPException(status_code=500, detail="An error occurred")
