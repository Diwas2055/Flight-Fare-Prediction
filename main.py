import uvicorn
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles

from routes import list_routes


def include_router(app):
    list_routes(app=app)


def configure_static(app):
    app.mount("/static", StaticFiles(directory="static"), name="static")


def middleware(app):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def start_application():
    app = FastAPI(
        title="Flight Fare Prediction",
        debug=False,
    )
    include_router(app)
    configure_static(app)
    return app


app = start_application()


@app.middleware("http")
async def verify_user_agent(request: Request, call_next):
    if request.headers["User-Agent"].find("Mobile") == -1:
        # function 'call_next' that will receive the 'request' as a parameter
        response = await call_next(request)
        return response
    else:
        return JSONResponse(
            content={"message": "we do not allow mobiles"}, status_code=401
        )


if __name__ == "__main__":
    uvicorn.run("main:app", port=9000, reload=True)
