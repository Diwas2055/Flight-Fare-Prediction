# **Project: Flight Fare Prediction**

## **Overview**
With the development of transportation industry and everyone has limited time for traveling, the airplane become more popular way of traveling. The advent of internet everywhere, people can easily check, book and buy air ticket any time with the help of internet. Airlines frequently changes prices of flights to maximize profit and fill more seats.  Based on the demand for specific route and travel date, flight distances, service categories, airline may increase or decrease the price of ticket. Generally, costumers want to busy ticket at lowest possible price. However, they are still confuse when to buy ticket or not. Especially those people who are not frequently travel in airplane. To overcome this problem, we are used machine learning model to predict flight price accurately. This would be very useful application for us, which can help passengers to make appropriate purchase decision and save their expense.

##  **Built With**

<img src="https://user-images.githubusercontent.com/50701303/110054888-0b14ce00-7d84-11eb-9d2a-fcec1cbe282a.jpg" width="100" height="100"/>

<img src="https://user-images.githubusercontent.com/50701303/110054671-a2c5ec80-7d83-11eb-806b-6fabe3c6141a.png" width="100" height="100"/>

<img src="https://user-images.githubusercontent.com/50701303/110055120-7b235400-7d84-11eb-9f63-0b4b63ee26e7.png" width="100" height="100"/>

<img src="https://user-images.githubusercontent.com/50701303/110055270-cb021b00-7d84-11eb-923a-0d2ea158adf7.png" width="100" height="100"/>

<img src="https://user-images.githubusercontent.com/50701303/110053960-5cbc5900-7d82-11eb-98f4-0ebe26222aa0.png" width="100" height="100"/>


## Requirements
* Python 3.7
*  Poetry 


> Note : If you don't have poetry installed, you can install it by running the following command in your terminal:
```bash
curl -sSL https://install.python-poetry.org | python3 - && 
echo 'export PATH="$HOME/.poetry/bin:$PATH"' >> ~/.bashrc &&
source ~/.bashrc
```



## Installation

1. Clone the repository
```bash
git clone <repo-url> flight-fare-prediction
```

2. Install the dependencies
```bash
poetry install
```

3. Train the model

- file path=`train_dataset/flight_fare_train.ipynb`
- Open the file in `jupyter notebook` and run the cells to train the model
- It will save the model in the `train_model` directory as `flight_fare_prediction_model.pkl`
- You can also use the pre-trained model in the `train_model` directory

4. After training the model, you can run the app by running the following command in your terminal
```bash
poetry run python main.py
```
