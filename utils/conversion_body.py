import urllib.parse


def convert_encoded_into_string(url_encoded_string):
    # Decode the URL-encoded string
    decoded_string = urllib.parse.unquote(url_encoded_string)

    # Split the string into key-value pairs
    pairs = decoded_string.split("&")

    # Create a dictionary from the key-value pairs
    data = {}
    for pair in pairs:
        key, value = pair.split("=")
        data[key] = value

    # Convert the dictionary to JSON
    json_data = data
    return json_data
